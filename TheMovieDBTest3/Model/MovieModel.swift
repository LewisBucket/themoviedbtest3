//
//  MovieModel.swift
//  TheMovieDB_Test
//
//  Created by Pedro Ferreira on 29/07/21.
//

import Foundation

struct Constants {
    static let apiKey = "e6dc8c20ea0d4c49874b8fa5173a1309"
    static let baseURl = "https://api.themoviedb.org/3"
    static let imagesBaseUrl = "https://image.tmdb.org/t/p/w500"
}

struct MovieList: Identifiable, Decodable {
    var backdrop_path: String?
    var id: Int?
    var genres: [Genre]?
    var overview: String?
    var poster_path: String?
    var release_date: String?
    var runtime: Int?
    var title: String?
}

struct DiscoverResponse: Decodable {
    var page: Int?
    var results: [MovieList]?
    var totalResults: Int?
    var totalPages: Int?
}

struct ErrorResponse: Decodable {
    var status_message: String?
    var status_code: Int?
}

struct Genre: Decodable {
    var id: Int?
    var name: String?
}

