//
//  TaskListViewModel.swift
//  TheMovieDB_Test
//
//  Created by Pedro Ferreira on 29/07/21.
//

import Foundation

class TalkListViewModel: ObservableObject {
    
    @Published var saveData: String = ""
    @Published var tasks: [TaskViewModel] = []
    
    func getAllTasks(){
        tasks =  CoreDataManager.shared.getAllTasks().map(TaskViewModel.init)
    }
    
    func save() {
        let task = Task(context: CoreDataManager.shared.persistentContainer.viewContext)
        task.saveData = saveData
        CoreDataManager.shared.save()
    }
}

struct TaskViewModel {
    let task: Task
    var idTask: NSObject {
        return task.objectID
    }
    var saveData: String {
        return task.saveData ?? ""
    }
}

