//
//  TaskListCoreDataView.swift
//  TheMovieDB_Test
//
//  Created by Pedro Ferreira on 29/07/21.
//

import SwiftUI

struct CoreDataView: View {
    @StateObject private var taskListVM = TalkListViewModel()
    var body: some View {
        VStack{
            Text("Historial CoreData")
                .font(.title)
                .fontWeight(.bold)
                .foregroundColor(.yellow)
                .padding(.leading, 8)
            List(taskListVM.tasks, id: \.idTask) { task in
                Text(task.saveData)
            }
            .onAppear{taskListVM.getAllTasks()}
            Spacer()
           
        }.padding()
    }
}

struct TaskListCoreDataView_Previews: PreviewProvider {
    static var previews: some View {
        CoreDataView()
    }
}

