//
//  TheMovieDBTest3App.swift
//  TheMovieDBTest3
//
//  Created by Pedro Ferreira on 1/08/21.
//

import SwiftUI

@main
struct TheMovieDBTest3App: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
