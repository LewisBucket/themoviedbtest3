//
//  MovieListView.swift
//  TheMovieDB_Test
//
//  Created by Pedro Ferreira on 29/07/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct MoviesListView: View {
    @ObservedObject private var viewModel = HomeViewModel()
    @ObservedObject private var taskListVM = TalkListViewModel()
    @State var searchText = ""
    @State private var showModalSearchView = false
    
    var title: String
    var sortBy: String
    
    var orientation: String = "horizontal"
    
    var body: some View {
        VStack {
            NavigationLink(
                destination: MovieSearchView(),
                label: {
                    Text("Search Movie").foregroundColor(.black)
                    Image(systemName: "magnifyingglass.circle")
                        .font(.system(size: 36, weight: .bold))
                        .font(.title2).accentColor(.yellow)
                        .shadow(color: Color.black, radius: 5)
                    
                })
            
            HStack {
                Text(title)
                    .font(.title2)
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                
                Spacer()
            }
            .padding(.horizontal)
            
            
            ScrollView(.horizontal, showsIndicators: false) {
                VStack(spacing: 15) {
                    ForEach(viewModel.items ?? viewModel.placeholders, id: \.id) { item in
                        NavigationLink(
                            destination: MovieDetailsView(item: item),
                            label: {
                                MovieItemView(item: item, orientation: orientation)
                                Spacer(minLength: 50)
                                Button {
                                    print(item.title!)
                                    taskListVM.saveData = item.title!
                                    taskListVM.save()
                                    print(taskListVM.saveData)
                                } label: {
                                    Text("See Later").foregroundColor(.black)
                                    Image(systemName: "bookmark").font(.title2).accentColor(.yellow)
                                }
                            })
                        
                    }
                }
                .padding(.horizontal)
            }
            
            Spacer()
        }
        .onAppear {
            viewModel.fetchData(sortBy: sortBy)
        }
    }
}

struct MovieItemView: View {
    var item: MovieList
    var orientation: String = "horizontal"
    
    var body: some View {
        VStack {
            WebImage(url: URL(string: "\(Constants.imagesBaseUrl)\((orientation == "horizontal" ? item.backdrop_path : item.poster_path) ?? "")"))
                .resizable()
                .scaledToFill()
                .frame(width: orientation == "horizontal" ? 200 : 160, height: orientation == "horizontal" ? 120 : 240)
                .redacted(reason: item.poster_path == nil ? .placeholder : .init())
                .cornerRadius(8)
            
            HStack {
                VStack(alignment: .leading, spacing: 4) {
                    Text(item.title ?? "Cargando...")
                        .font(.system(size: orientation == "horizontal" ? 17 : 15))
                        .fontWeight(.bold)
                        .foregroundColor(.black)
                        .redacted(reason: item.title == nil ? .placeholder : .init())
                    
                    Text(item.overview ?? "Cargando...")
                        .font(.system(size: orientation == "horizontal" ? 15 : 13))
                        .lineLimit(2)
                        .foregroundColor(.gray)
                        .redacted(reason: item.overview == nil ? .placeholder : .init())
                }
                
                Spacer()
            }
        }
        .frame(width: orientation == "horizontal" ? 200 : 160)
    }
}

struct MoviesListView_Previews: PreviewProvider {
    static var previews: some View {
        MoviesListView(title: "TITLE", sortBy: "popularity.asc")
    }
}

