//
//  HomeView.swift
//  TheMovieDB_Test
//
//  Created by Pedro Ferreira on 29/07/21.
//

import SwiftUI
import SDWebImageSwiftUI

struct HomeView: View {
    
    init() {
        UINavigationBar.appearance().barTintColor = .clear
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
    }
    @State private var showModalView = false
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false, content: {
                VStack {
                    FeaturedMoviesView()
                    MoviesListView(title: "Popular", sortBy: "popularity.desc")

                }
                .padding(.bottom, UIApplication.shared.windows.first?.safeAreaInsets.bottom)
            })
            .background(Color(.white))
            .ignoresSafeArea(.all, edges: .all)
            .navigationBarItems(trailing: Button {
                showModalView = true
            } label: {
                Image(systemName: "bookmark.fill").font(.title2).accentColor(.yellow)
                
            })
            .sheet(isPresented: $showModalView) {
                
                RecentsView()
        }
            
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

