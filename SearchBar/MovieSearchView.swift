//
//  MovieSearchView.swift
//  SearchBarMovie
//
//  Created by Pedro Ferreira on 31/07/21.
//

import SwiftUI
let coloredNavAppearance = UINavigationBarAppearance()

struct MovieSearchView: View {
    
    @ObservedObject var movieSearchState = MovieSearchState()
    
    var body: some View {
        NavigationView {
            List {
                SearchBarView(placeholderSearchBar: "Search movies", text: self.$movieSearchState.query)
                    .listRowInsets(EdgeInsets(top: 0, leading: 8, bottom: 0, trailing: 8))
                
                LoadingView(isLoading: self.movieSearchState.isLoading, errorLoading: self.movieSearchState.errorSearch) {
                    self.movieSearchState.search(query: self.movieSearchState.query)
                }
                
                if self.movieSearchState.moviesSearch != nil {
                    ForEach(self.movieSearchState.moviesSearch!) { movie in
                        NavigationLink(destination: MovieDetailView(movieId: movie.id)) {
                            VStack(alignment: .leading) {
                                Text(movie.title)
                                Text(movie.yearText)
                            }
                        }
                    }
                }
                
            }.background(Color.red)
            .onAppear {
                self.movieSearchState.startObserve()
            }
            .navigationBarTitle("Search")
            
        }
    }
}

struct MovieSearchView_Previews: PreviewProvider {
    static var previews: some View {
        MovieSearchView()
    }
}

