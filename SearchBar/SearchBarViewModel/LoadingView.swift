//
//  LoadingView.swift
//  SearchBarMovie
//
//  Created by Pedro Ferreira on 31/07/21.
// Pantalla de Carga

import SwiftUI

struct LoadingView: View {
    
    let isLoading: Bool
    let errorLoading: NSError?
    let retryAction: (() -> ())?
    
    var body: some View {
        Group {
            if isLoading {
                HStack {
                    Spacer()
                    ActivityIndicatorView()
                    Spacer()
                }
            } else if errorLoading != nil {
                HStack {
                    Spacer()
                    VStack(spacing: 4) {
                        Text(errorLoading!.localizedDescription).font(.headline)
                        if self.retryAction != nil {
                            Button(action: self.retryAction!) {
                                Text("Retry")
                            }
                            .foregroundColor(Color.blue)
                            .buttonStyle(PlainButtonStyle())
                        }
                    }
                    Spacer()
                }
            }
        }
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView(isLoading: true, errorLoading: nil, retryAction: nil)
    }
}

