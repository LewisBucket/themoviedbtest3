//
//  Movie+Stub.swift
//  SearchBarMovie
//
//  Created by Pedro Ferreira on 31/07/21.
//

import Foundation


extension MovieSearch {
    
    static var stubbedMovies: [MovieSearch] {
        let response: MovieResponse? = try? Bundle.main.loadAndDecodeJSON(filename: "movie_list")
        return response!.results
    }
    
    static var stubbedMovie: MovieSearch {
        stubbedMovies[0]
    }
    
}

extension Bundle {
    
    func loadAndDecodeJSON<D: Decodable>(filename: String) throws -> D? {
        guard let url = self.url(forResource: filename, withExtension: "json") else {
            return nil
        }
        let data = try Data(contentsOf: url)
        let jsonDecoder = Utils.jsonDecoderSearch
        let decodedModel = try jsonDecoder.decode(D.self, from: data)
        return decodedModel
    }
}

